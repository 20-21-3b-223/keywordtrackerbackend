using Fluxor;

namespace KeywordTrackerFrontend.Store.LocalStorage
{
    public static class Reducers
    {
        [ReducerMethod]
        public static LocalStorageState ReduceLoadStorage(LocalStorageState state, LoadStorageAction action)
        {
            return new()
            {
                isLoading = true
            };
        }

        [ReducerMethod]
        public static LocalStorageState ReduceLocalStorageLoaded(LocalStorageState state,
            LocalStorageLoadedAction action)
        {
            return new()
            {
                isLoading = false
            };
        }
    }
}