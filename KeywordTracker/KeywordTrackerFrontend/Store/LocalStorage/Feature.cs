using Fluxor;

namespace KeywordTrackerFrontend.Store.LocalStorage
{
    public class Feature : Feature<LocalStorageState>
    {
        public override string GetName()
        {
            return "LocalStorage";
        }

        protected override LocalStorageState GetInitialState()
        {
            return new()
            {
                isLoading = false
            };
        }
    }
}