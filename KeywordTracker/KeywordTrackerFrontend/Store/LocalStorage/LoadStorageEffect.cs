using System.Threading.Tasks;
using Blazored.LocalStorage;
using Fluxor;

namespace KeywordTrackerFrontend.Store.LocalStorage
{
    public class LoadStorageEffect : Effect<LoadStorageAction>
    {
        private ILocalStorageService _localStorage;

        public LoadStorageEffect(ILocalStorageService localStorage)
        {
            _localStorage = localStorage;
        }

        public override async Task HandleAsync(LoadStorageAction action, IDispatcher dispatcher)
        {
            dispatcher.Dispatch(new LocalStorageLoadedAction());
        }
    }
}