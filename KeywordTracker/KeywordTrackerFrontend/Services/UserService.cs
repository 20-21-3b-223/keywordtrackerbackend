﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using Keywordtracker.Shared.Models;
using KeywordTrackerFrontend.Api;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;

namespace KeywordTrackerFrontend.Services
{
    public class UserService : IUserService
    {
        private readonly AuthenticationStateProvider _authenticationStateProvider;
        private readonly HttpClient _httpClient;
        private readonly ILocalStorageService _localStorage;

        public UserService(HttpClient httpClient,
            AuthenticationStateProvider authenticationStateProvider,
            ILocalStorageService localStorage)
        {
            _httpClient = httpClient;
            _authenticationStateProvider = authenticationStateProvider;
            _localStorage = localStorage;
        }

        public async Task<StandartResult> Register(RegisterModel registerModel)
        {
            var result =
                await _httpClient.PostJsonAsync<StandartResult>("https://localhost:6001/api/Account/register",
                    registerModel);
            return result;
        }

        public async Task<LoginResult> Login(LoginModel loginModel)
        {
            var loginAsJson = JsonSerializer.Serialize(loginModel);
            var response = await _httpClient.PostAsync("https://localhost:6001/api/Account/login",
                new StringContent(loginAsJson, Encoding.UTF8, "application/json"));
            var loginResult = JsonSerializer.Deserialize<LoginResult>(await response.Content.ReadAsStringAsync(),
                new JsonSerializerOptions {PropertyNameCaseInsensitive = true});

            if (!response.IsSuccessStatusCode) return loginResult;

            await _localStorage.SetItemAsync("authToken", loginResult.Token);
            ((ApiAuthenticationStateProvider) _authenticationStateProvider).MarkUserAsAuthenticated(loginModel
                .Email);
            _httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("bearer", loginResult.Token);

            return loginResult;
        }

        public async Task ForgotPassword(ForgotPasswordModel model)
        {
            await _httpClient.PostJsonAsync("https://localhost:6001/api/Account/forgotpassword", model);
        }

        public async Task<ResetPasswordResult> ResetPassword(ResetPasswordModel model)
        {
            var result =
                await _httpClient.PostJsonAsync<ResetPasswordResult>(
                    "https://localhost:6001/api/Account/resetpassword", model);
            return result;
        }

        public async Task<StandartResult> ConfirmPassword(ConfirmPasswordModel model)
        {
            var result =
                await _httpClient.PostJsonAsync<StandartResult>(
                    "https://localhost:6001/api/Account/confirmpassword", model);
            return result;
        }

        public async Task<EmailNameModel> GetNameAndEmail() {
            var result = await _httpClient.PostJsonAsync<EmailNameModel>(
                    "https://localhost:6001/api/Account/GetUserInformation", null);
            return result;
        }

        public async Task<StandartResult> SetNewPassword(SetPasswordModel model){
            var result = await _httpClient.PostJsonAsync<StandartResult>(
                "https://localhost:6001/api/Account/SetNewPassword", model);
            return result;
        }

        public async Task RequestNewEmail(RequestEmailModel model) {
            Console.WriteLine($"Requesting: {model.NewEmail}");
            await _httpClient.PostJsonAsync("https://localhost:6001/api/Account/RequestNewEmail", model);
        }

        public async Task<StandartResult> SetNewEmail(SetEmailModel model) {
            var result = await _httpClient.PostJsonAsync<StandartResult>(
                "https://localhost:6001/api/Account/SetNewEmail", model);
            return result;
        }

        public async Task<StandartResult> DeleteUser(DeleteUserModel model) {
            var result = await _httpClient.PostJsonAsync<StandartResult>(
                "https://localhost:6001/api/Account/DeleteUser", model);
            return result;
        }

        public async Task Logout()
        {
            await _localStorage.RemoveItemAsync("authToken");
            ((ApiAuthenticationStateProvider) _authenticationStateProvider).MarkUserAsLoggedOut();
            _httpClient.DefaultRequestHeaders.Authorization = null;
        }
    }
}