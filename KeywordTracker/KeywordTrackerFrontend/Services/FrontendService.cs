﻿using System.Net.Http;
using System.Threading.Tasks;
using Keywordtracker.Shared.Models;
using Microsoft.AspNetCore.Components;

namespace KeywordTrackerFrontend.Services
{

    public class FrontendService : IFrontendService
    {
        private HttpClient _client;
        
        public FrontendService(HttpClient client)
        {
            this._client = client;
        }
        
        public async Task AddKeyword(CreateKeywordModel model)
        {
            await _client.PostJsonAsync("https://localhost:6001/addkeyword", model);
        }

        public async Task DeleteKeyword(DeleteKeywordModel model)
        {
            await _client.PostJsonAsync("https://localhost:6001/deletekeyword", model);
        }

        public Task<GetKeywordResult> GetKeywords()
        {
            return _client.GetJsonAsync<GetKeywordResult>("https://localhost:6001/getkeyword");
        }
    }
}