using System.Threading.Tasks;
using Keywordtracker.Shared.Models;

namespace KeywordTrackerFrontend.Services
{
    public interface IWebsiteService
    {
        Task<CreateWebsiteResult> Create(CreateWebsiteModel model);
        
        Task<ReadWebsiteResult> Read();
        
        Task Delete(DeleteWebsiteModel model);
    }
}