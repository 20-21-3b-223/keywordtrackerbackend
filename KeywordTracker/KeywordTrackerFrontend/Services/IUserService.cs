﻿using System.Threading.Tasks;
using Keywordtracker.Shared.Models;

namespace KeywordTrackerFrontend.Services
{
    public interface IUserService
    {
        Task<LoginResult> Login(LoginModel loginModel);
        Task Logout();
        Task<StandartResult> Register(RegisterModel registerModel);
        Task ForgotPassword(ForgotPasswordModel model);
        Task<ResetPasswordResult> ResetPassword(ResetPasswordModel model);
        Task<StandartResult> ConfirmPassword(ConfirmPasswordModel model);
        Task<EmailNameModel> GetNameAndEmail();
        Task<StandartResult> SetNewPassword(SetPasswordModel model);
        Task RequestNewEmail(RequestEmailModel model);
        Task<StandartResult> SetNewEmail(SetEmailModel model);
        Task<StandartResult> DeleteUser(DeleteUserModel model);
    }
}