using System.Net.Http;
using System.Threading.Tasks;
using Keywordtracker.Shared.Models;
using Microsoft.AspNetCore.Components;

namespace KeywordTrackerFrontend.Services
{
    public class WebsiteService : IWebsiteService
    {
        private HttpClient _client;
        
        public WebsiteService(HttpClient client)
        {
            this._client = client;
        }
        
        public async Task<CreateWebsiteResult> Create(CreateWebsiteModel model)
        {
            return await this._client.PutJsonAsync<CreateWebsiteResult>("https://localhost:6001/api/website/create", model);
        }

        public async Task<ReadWebsiteResult> Read()
        {
            return await this._client.GetJsonAsync<ReadWebsiteResult>("https://localhost:6001/api/website/read");
        }

        public async Task Delete(DeleteWebsiteModel model)
        {
            await _client.PostJsonAsync("https://localhost:6001/api/website/delete", model);
        }
    }
}