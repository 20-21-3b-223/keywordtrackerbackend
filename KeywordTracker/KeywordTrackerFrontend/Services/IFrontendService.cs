﻿using System.Threading.Tasks;
using Keywordtracker.Shared.Models;

namespace KeywordTrackerFrontend.Services
{
    public interface IFrontendService
    {
        Task AddKeyword(CreateKeywordModel model);

        Task<GetKeywordResult> GetKeywords();

        Task DeleteKeyword(DeleteKeywordModel model);
    }
}