﻿using System.ComponentModel.DataAnnotations;
using Keywordtracker.Shared.Models;

namespace KeywordTrackerFrontend.Shared
{
    public class ClientSideSetPasswordModel : SetPasswordModel
    {
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("NewPassword", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}