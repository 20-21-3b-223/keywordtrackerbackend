﻿using System.ComponentModel.DataAnnotations;
using Keywordtracker.Shared.Models;

namespace KeywordTrackerFrontend.Shared
{
    public class ClientSideRegisterModel : RegisterModel
    {
            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
    }
}