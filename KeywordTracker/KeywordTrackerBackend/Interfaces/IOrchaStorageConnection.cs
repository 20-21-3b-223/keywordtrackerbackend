using KeywordTrackerBackend.Entity;

namespace KeywordTrackerBackend.Interfaces
{
    public interface IOrchaStorageConnection
    {
        public User GetUser(int id);
        public User CreateUser(User user);
        public void UpdateUser(User user);
        public void DeleteUser(User user);
    }
}