namespace KeywordTrackerBackend.Interfaces
{
    public interface IWebsiteLoader
    {
        public string LoadWebsite(string url);
    }
}