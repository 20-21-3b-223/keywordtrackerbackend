using KeywordTrackerBackend.Enums;

namespace KeywordTrackerBackend.Interfaces
{
    public interface IOrchaSystem
    {
        EOrchaStatus Status { get; }

        public void Start();
        public void Stop();
    }
}