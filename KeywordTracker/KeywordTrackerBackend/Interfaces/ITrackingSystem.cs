using System;
using KeywordTrackerBackend.Classes;
using KeywordTrackerBackend.Records;

namespace KeywordTrackerBackend.Interfaces
{
    public interface ITrackingSystem : IOrchaSystem
    {
        public int Interval { get; set; }

        public XChannel<TrackingResult> TrackingResultOutChannel { get; }

        public event EventHandler<TrackingResult> TrackingResultFound;
    }
}