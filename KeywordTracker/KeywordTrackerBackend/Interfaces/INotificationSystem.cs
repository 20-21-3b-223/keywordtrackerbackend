﻿using KeywordTrackerBackend.Classes;

namespace KeywordTrackerBackend.Interfaces
{
    public interface INotificationSystem : IOrchaSystem
    {
        public XChannel<NotificationRequest> IncomingRequests { get; }
    }
}