using System;
using System.Threading.Tasks;
using MimeKit;

namespace KeywordTrackerBackend.Interfaces
{
    public interface IEmailClient
    {
        public EventHandler<MimeMessage> OnSendEmail { get; set; }
        public Task SendEmail(MimeMessage message);

        public void Connect(string server, int port = 465);
        public void Disconnect();
        public void Authenticate(string email, string password);
    }
}