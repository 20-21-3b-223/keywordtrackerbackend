﻿using System.Threading.Tasks;

namespace KeywordTrackerBackend.EmailService
{
    public interface IEmailSender
    {
        void SendEmail(Message message);
        Task SendEmailAsync(Message message);
    }
}