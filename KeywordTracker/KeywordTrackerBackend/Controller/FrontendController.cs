﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Keywordtracker.Shared.Models;
using KeywordTrackerBackend.Classes;
using KeywordTrackerBackend.Domain;
using KeywordTrackerBackend.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace KeywordTrackerBackend.Controller
{    
    [Authorize]
    [ApiController]
    public class FrontendController : ControllerBase
    {
        private IServiceProvider provider;
        
        public FrontendController(IServiceProvider provider)
        {
            this.provider = provider;
        }
        
        
        [HttpPost]
        [Route("/addkeyword")]
        public async Task<IActionResult> Create([FromBody] CreateKeywordModel m)
        {
            int keywordId = -1;
            int websiteId = -1;

            if (!UrlHelpers.ValidHttpURL(m.WebsiteURL))
            {
                Console.WriteLine("Invalid URL");
                return this.BadRequest("URL Invalid");
            }

            var userID = User .Claims.Where(a => a.Type == ClaimTypes.PrimarySid).FirstOrDefault().Value;


            User user;
            using var repo = this.provider.GetService(typeof(ARepository<User>)) as ARepository<User>;
            {
                user = repo.Read(userID);
            }


            CreateConfigurationResult result = new CreateConfigurationResult();
            IEnumerable<Keyword> foundKeyword;
            bool fKeyword = false;
            using var keywordRepo = this.provider.GetService(typeof(ARepository<Keyword>)) as ARepository<Keyword>;
            {
                foundKeyword = keywordRepo.Get(keyword => keyword.Value == m.KeywordValue);
            }
            if (foundKeyword.Count() > 0)
            {
                fKeyword = true;
                keywordId = foundKeyword.FirstOrDefault().Id;
            }

            Keyword keyword = new Keyword();
            if (!fKeyword)
            {
                keyword = keywordRepo.Create(new Keyword()
                {
                    Value = m.KeywordValue
                });
            }
            
            
            IEnumerable<Website> foundWebsite;
            bool wFound = false;
            using var websiteRepo = this.provider.GetService(typeof(ARepository<Website>)) as ARepository<Website>;
            {
                foundWebsite = websiteRepo.Get(website => website.URL == m.WebsiteURL);
            }
            if (foundWebsite.Count() > 0)
            {
                wFound = true;
                websiteId = foundWebsite.FirstOrDefault().Id;
            }

            Website website = new Website();
            if (!wFound)
            {
                website = websiteRepo.Create(new Website()
                {
                    URL = m.WebsiteURL
                });
            }
      

            ConfigurationJT config;
            using var configRepo = this.provider.GetService(typeof(ARepository<ConfigurationJT>)) as ARepository<ConfigurationJT>;
            {
                config = configRepo.Create(new ConfigurationJT()
                {
                    WebsiteId = (websiteId == -1) ? website.Id : websiteId,
                    KeywordId = keywordId == -1 ? keyword.Id : keywordId,
                    UserId = userID
                });
            }

            result = new CreateConfigurationResult()
            {
                WebsiteId = (websiteId == -1) ? website.Id : websiteId,
                KeywordId = keywordId == -1 ? keyword.Id : keywordId,
                UserId = userID
            };


           
            return this.Ok(result);
        }

        [HttpGet]
        [Route("/getkeyword")]
        public async Task<IActionResult> Get()
        {
            var userID = User.Claims.Where(a => a.Type == ClaimTypes.PrimarySid).FirstOrDefault().Value;

            var result = new GetKeywordResult();

            IEnumerable<ConfigurationJT> userSites;
            List<Tuple<int, int>> userSitesIDS = new List<Tuple<int, int>>();
            using var configRepo = this.provider.GetService(typeof(ARepository<ConfigurationJT>)) as ARepository<ConfigurationJT>;
            {
                userSites = configRepo.Get(jt => jt.UserId == userID);
                foreach (var site in userSites)
                {
                    userSitesIDS.Add(new Tuple<int, int>(site.WebsiteId, site.KeywordId));
                }
            }

            List<KeywordResult> listresult = new List<KeywordResult>();
            foreach (var site in userSitesIDS)
            {
                Website website;
                using var context = this.provider.GetService(typeof(KeywordTrackerDbContext)) as KeywordTrackerDbContext;
                {
                    website = context.Websites.Find(site.Item1);
                }
                
                Keyword keyword;
                using var keywordRepo = this.provider.GetService(typeof(ARepository<Keyword>)) as ARepository<Keyword>;
                {
                    var x = keywordRepo.Get(keyword1 => keyword1.Id == site.Item2);
                    keyword = x.FirstOrDefault();
                }

                listresult.Add(new KeywordResult(website.URL, keyword.Value));
            }
            result = new GetKeywordResult()
            {
                Results = listresult
            };
            
            

            return this.Ok(result);
        }

        [HttpPost]
        [Route("/deletekeyword")]
        public async Task<IActionResult> Delete([FromBody] DeleteKeywordModel model)
        {
            var userID = User.Claims.Where(a => a.Type == ClaimTypes.PrimarySid).FirstOrDefault().Value;

            using (var ctx = this.provider.GetService(typeof(KeywordTrackerDbContext)) as KeywordTrackerDbContext)
            {
                var websiteID = (from w in ctx.Websites
                    where w.URL == model.Website
                    select w.Id).FirstOrDefault();

                var keywordID = (from k in ctx.Keywords
                    where k.Value == model.Keyword
                    select k.Id).FirstOrDefault();

                var configuration = (from c in ctx.ConfigurationJTs
                    where c.UserId == userID && c.KeywordId == keywordID && c.WebsiteId == websiteID
                    select c).FirstOrDefault();

                ctx.Remove(configuration);
                ctx.SaveChanges();
            }
            return this.Ok();
        }
    }
}