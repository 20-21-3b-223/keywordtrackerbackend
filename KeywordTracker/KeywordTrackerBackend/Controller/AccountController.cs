using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using Keywordtracker.Shared.Models;
using KeywordTrackerBackend.Classes;
using KeywordTrackerBackend.EmailService;
using KeywordTrackerBackend.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace KeywordTrackerBackend.Controller {
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase {
        private readonly IConfiguration _configuration;
        private readonly IEmailSender _emailSender;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly IServiceProvider _provider;


        public AccountController(UserManager<User> userManager, RoleManager<IdentityRole> roleManager,
            IConfiguration configuration, SignInManager<User> signInManager, IEmailSender emailSender,
            IServiceProvider provider) {
            _userManager = userManager;
            _roleManager = roleManager;
            _configuration = configuration;
            _signInManager = signInManager;
            _emailSender = emailSender;
            this._provider = provider;
        }


        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginModel model) {
            var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, false, false);

            if (!result.Succeeded)
                return BadRequest(new LoginResult
                    {Successful = false, Error = "Username and password are invalid."});

            var user = await _userManager.FindByEmailAsync(model.Email);

            var claims = new[] {
                new Claim(ClaimTypes.Email, model.Email),
                new Claim(ClaimTypes.Name, (user.FirstName + " " + user.LastName)),
                new Claim(ClaimTypes.PrimarySid, user.Id)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtSecurityKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expiry = DateTime.Now.AddDays(Convert.ToInt32(_configuration["JwtExpiryInDays"]));

            var token = new JwtSecurityToken(_configuration["JwtIssuer"], _configuration["JwtAudience"],
                claims,
                expires: expiry,
                signingCredentials: creds
            );

            Console.WriteLine(token);

            return Ok(new LoginResult
                {Successful = true, Token = new JwtSecurityTokenHandler().WriteToken(token)});
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegisterModel model) {
            var r = new Regex("^[a-zA-Z]*$");

            if (!r.IsMatch(model.FirstName)) {
                IEnumerable<string> errors = new[] {"Please use alphanumeric Letter for First Name"};

                return Ok(new StandartResult {Successful = false, Errors = errors});
            }

            if (!r.IsMatch(model.LastName)) {
                IEnumerable<string> errors = new[] {"Please use alphanumeric Letter for Last Name"};

                return Ok(new StandartResult {Successful = false, Errors = errors});
            }

            var newUser = new User {
                UserName = model.Email,
                Email = model.Email,
                FirstName = model.FirstName.Substring(0, 1).ToUpper() + model.FirstName.Substring(1).ToLower(),
                LastName = model.LastName.Substring(0, 1).ToUpper() + model.LastName.Substring(1).ToLower(),
                JoinDate = DateTime.Today
            };

            var result = await _userManager.CreateAsync(newUser, model.Password);

            if (!result.Succeeded) {
                var errors = result.Errors.Select(x => x.Description);

                return Ok(new StandartResult {Successful = false, Errors = errors});
            }

            var token = await _userManager.GenerateEmailConfirmationTokenAsync(newUser);

            var uriBuilder = new UriBuilder("https://localhost:5001/ConfirmPassword");
            var parameters = HttpUtility.ParseQueryString(string.Empty);
            parameters["token"] = token;
            parameters["email"] = newUser.Email;
            uriBuilder.Query = parameters.ToString();
            var confirmationLink = uriBuilder.Uri.ToString();

            var template = new EmailTemplate("Templates/email-button.html");

            template.AddMapping("FIRSTNAME", newUser.FirstName);
            template.AddMapping("CONFIRMMAILCONTENT", confirmationLink);


            var message = new Message(new[] {newUser.Email}, "Confirmation email link", template.Render(), null);
            await _emailSender.SendEmailAsync(message);

            return Ok(new StandartResult {Successful = true});
        }

        [HttpPost]
        [Route("confirmpassword")]
        public async Task<IActionResult> ConfirmEmail(ConfirmPasswordModel model) {
            var user = await _userManager.FindByEmailAsync(model.Email);
            Console.WriteLine(model.Token);

            if (user == null)
                return Ok(new StandartResult {Successful = false, Errors = new[] {"Error"}});

            var result = await _userManager.ConfirmEmailAsync(user, model.Token);
            if (!result.Succeeded)
                return Ok(new StandartResult {Successful = false, Errors = result.Errors.Select(x => x.Description)});

            return Ok(new StandartResult {Successful = true});
        }

        [HttpPost]
        [Route("forgotpassword")]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordModel forgotPasswordModel) {
            if (!ModelState.IsValid)
                return Ok(new ResetPasswordResult {Successful = false, Error = new[] {"Invalid Request"}});

            var user = await _userManager.FindByEmailAsync(forgotPasswordModel.Email);
            if (user == null)
                return Ok();

            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            var callback = Url.Action(null, null, new {token, email = user.Email}, Request.Scheme);

            var uriBuilder = new UriBuilder("https://localhost:5001/ResetPassword");
            var parameters = HttpUtility.ParseQueryString(string.Empty);
            parameters["token"] = token;
            parameters["email"] = user.Email;
            uriBuilder.Query = parameters.ToString();
            var finalUrl = uriBuilder.Uri.ToString();

            var template = new EmailTemplate("Templates/reset-password.html");

            template.AddMapping("FIRSTNAME", user.FirstName);
            template.AddMapping("CONFIRMMAILCONTENT", finalUrl);


            var message = new Message(new[] {user.Email}, "Reset Password", template.Render(), null);
            await _emailSender.SendEmailAsync(message);
            return Ok();
        }

        [HttpPost]
        [Route("resetpassword")]
        public async Task<IActionResult> ResetPassword(ResetPasswordModel resetPasswordModel) {
            if (!ModelState.IsValid)
                return Ok(new ResetPasswordResult {Successful = false, Error = new[] {"Invalid Request"}});

            var user = await _userManager.FindByEmailAsync(resetPasswordModel.Email);
            if (user == null)
                return Ok(new ResetPasswordResult {Successful = false, Error = new[] {"Invalid Request"}});

            var resetPassResult =
                await _userManager.ResetPasswordAsync(user, resetPasswordModel.Token, resetPasswordModel.Password);
            if (!resetPassResult.Succeeded)
                return Ok(new ResetPasswordResult
                    {Successful = false, Error = new[] {resetPassResult.Errors.ToString()}});

            return Ok(new ResetPasswordResult {Successful = true});
        }

        [HttpPost]
        [Route("GetUserInformation")]
        [Authorize]
        public async Task<IActionResult> GetUserInformation() {
            if (!ModelState.IsValid)
                return Ok(new ResetPasswordResult {Successful = false, Error = new[] {"Invalid Request"}});

            string userEmail = User.Claims.Where(a => a.Type == ClaimTypes.Email).FirstOrDefault().Value;

            if (String.IsNullOrEmpty(userEmail))
                return Ok(new ResetPasswordResult {Successful = false, Error = new[] {"Invalid Request"}});

            var user = await _userManager.FindByEmailAsync(userEmail);
            if (user == null)
                return Ok(new ResetPasswordResult {Successful = false, Error = new[] {"Invalid Request"}});

            // await using (_context) {
            //     int entrys = (from jt in _context.ConfigurationJTs
            //         where jt.UserId == user.Id
            //         select jt.KeywordId).Count();
            // }

            return Ok(new EmailNameModel()
                {Email = user.Email, FirstName = user.FirstName, LastName = user.LastName, JoinedAt = user.JoinDate});
        }


        [HttpPost]
        [Route("SetNewPassword")]
        [Authorize]
        public async Task<IActionResult> SetNewPassword(SetPasswordModel model) {
            if (!ModelState.IsValid)
                return Ok(new ResetPasswordResult {Successful = false, Error = new[] {"Invalid Request"}});

            string userEmail = User.Claims.Where(a => a.Type == ClaimTypes.Email).FirstOrDefault().Value;
            if (String.IsNullOrEmpty(userEmail))
                return Ok(new ResetPasswordResult {Successful = false, Error = new[] {"Invalid Request"}});

            var user = await _userManager.FindByEmailAsync(userEmail);
            if (user == null)
                return Ok(new ResetPasswordResult {Successful = false, Error = new[] {"Invalid Request"}});

            var result = await _signInManager.PasswordSignInAsync(user.UserName, model.Password, false, false);

            if (!result.Succeeded)
                return Ok(new StandartResult
                    {Successful = false, Errors = new[] {"Username and password are invalid."}});

            string token = await _userManager.GeneratePasswordResetTokenAsync(user);

            var success = await _userManager.ResetPasswordAsync(user, token, model.NewPassword);

            if (!success.Succeeded)
                return Ok(new StandartResult()
                    {Successful = false, Errors = success.Errors.Select(x => x.Description)});

            Console.WriteLine(success);

            return Ok(new StandartResult() {Successful = true});
        }


        [HttpPost]
        [Route("RequestNewEmail")]
        [Authorize]
        public async Task<IActionResult> RequestNewEmail(RequestEmailModel model) {
            if (!ModelState.IsValid)
                return Ok(new StandartResult() {Successful = false, Errors = new[] {"Invalid Request"}});

            string userEmail = User.Claims.Where(a => a.Type == ClaimTypes.Email).FirstOrDefault().Value;
            if (String.IsNullOrEmpty(userEmail))
                return Ok();

            var user = await _userManager.FindByEmailAsync(userEmail);
            if (user == null)
                return Ok();

            var token = await _userManager.GenerateChangeEmailTokenAsync(user, model.NewEmail);

            var uriBuilder = new UriBuilder("https://localhost:5001/SetNewEmail");
            var parameters = HttpUtility.ParseQueryString(string.Empty);
            parameters["token"] = token;
            parameters["email"] = user.Email;
            parameters["newemail"] = model.NewEmail;
            uriBuilder.Query = parameters.ToString();
            var finalUrl = uriBuilder.Uri.ToString();

            var template = new EmailTemplate("Templates/set-email.html");

            template.AddMapping("FIRSTNAME", user.FirstName);
            template.AddMapping("CONFIRMMAILCONTENT", finalUrl);
            template.AddMapping("OLDEMAIL", user.Email);
            template.AddMapping("NEWMAIL", model.NewEmail);


            var message = new Message(new[] {user.Email}, "Set new email", template.Render(), null);
            await _emailSender.SendEmailAsync(message);
            return Ok();
        }

        [HttpPost]
        [Route("SetNewEmail")]
        public async Task<IActionResult> SetNewPassword(SetEmailModel setEmailModel) {
            if (!ModelState.IsValid)
                return Ok(new StandartResult {Successful = false, Errors = new[] {"Invalid Request"}});

            var user = await _userManager.FindByEmailAsync(setEmailModel.Email);
            if (user == null)
                return Ok(new StandartResult {Successful = false, Errors = new[] {"Invalid Request"}});

            var setEmailResult =
                await _userManager.ChangeEmailAsync(user, setEmailModel.NewEmail, setEmailModel.Token);

            if (!setEmailResult.Succeeded)
                return Ok(new StandartResult()
                    {Successful = false, Errors = setEmailResult.Errors.Select(x => x.Description)});

            var setUserNameResult = await _userManager.SetUserNameAsync(user, setEmailModel.NewEmail);

            if (!setUserNameResult.Succeeded)
                return Ok(new StandartResult()
                    {Successful = false, Errors = setUserNameResult.Errors.Select(x => x.Description)});

            return Ok(new StandartResult {Successful = true});
        }

        [HttpPost]
        [Route("DeleteUser")]
        [Authorize]
        public async Task<ActionResult> DeleteConfirmed(DeleteUserModel model) {
            if (!ModelState.IsValid)
                return Ok(new StandartResult {Successful = false, Errors = new[] {"Invalid Request"}});

            string userEmail = User.Claims.Where(a => a.Type == ClaimTypes.Email).FirstOrDefault().Value;
            if (String.IsNullOrEmpty(userEmail))
                return Ok(new StandartResult {Successful = false, Errors = new[] {"Invalid Request"}});


            var user = await _userManager.FindByEmailAsync(userEmail);
            if (user == null)
                return Ok(new StandartResult {Successful = false, Errors = new[] {"Invalid Request"}});

            var loginResult = await _signInManager.PasswordSignInAsync(user.UserName, model.Password, false, false);

            if (!loginResult.Succeeded)
                return Ok(new StandartResult
                    {Successful = false, Errors = new[] {"password is invalid"}});

            await _signInManager.SignOutAsync();

            var logins = await _userManager.GetLoginsAsync(user);
            var rolesForUser = await _userManager.GetRolesAsync(user);

            using (var _context = this._provider.GetService(typeof(KeywordTrackerDbContext)) as KeywordTrackerDbContext)
            {
                await using (var transaction = await _context.Database.BeginTransactionAsync()) {
                    IdentityResult result = IdentityResult.Success;
                    foreach (var login in logins) {
                        result = await _userManager.RemoveLoginAsync(user, login.LoginProvider, login.ProviderKey);
                        if (result != IdentityResult.Success)
                            break;
                    }

                    if (result == IdentityResult.Success) {
                        foreach (var item in rolesForUser) {
                            result = await _userManager.RemoveFromRoleAsync(user, item);
                            if (result != IdentityResult.Success)
                                break;
                        }
                    }

                    if (result == IdentityResult.Success) {
                        result = await _userManager.DeleteAsync(user);
                        if (result == IdentityResult.Success)
                            await transaction.CommitAsync(); //only commit if user and all his logins/roles have been deleted  
                    }
                }
            }
         
            return Ok(new StandartResult(){Successful = true});
        }
    }
}