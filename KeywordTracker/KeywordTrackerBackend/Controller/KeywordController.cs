﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Keywordtracker.Shared.Models;
using KeywordTrackerBackend.Domain;
using KeywordTrackerBackend.EmailService;
using KeywordTrackerBackend.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using KeywordTrackerBackend.Classes;
using Microsoft.Extensions.Configuration;

namespace KeywordTrackerBackend.Controller
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class KeywordController : ControllerBase
    {
        private ARepository<Website> _websiteRepo;
        private ARepository<ConfigurationJT> _configRepo;
        private ARepository<User> _userRepo;
        private ARepository<Keyword> _keywordRepo;

        public KeywordController(ARepository<Website> websiteRepo, ARepository<ConfigurationJT> configRepo,
            ARepository<User> userRepo, ARepository<Keyword> keywordRepo)
        {
            _websiteRepo = websiteRepo;
            _configRepo = configRepo;
            _userRepo = userRepo;
            _keywordRepo = keywordRepo;
        }

        [HttpPut]
        [Route("create")]
        public async Task<IActionResult> Create([FromBody] CreateKeywordModel m)
        {
            int keywordId = -1;
            int websiteId = -1;

            if (!UrlHelpers.ValidHttpURL(m.WebsiteURL))
            {
                Console.WriteLine("Invalid URL");
                return this.BadRequest("URL Invalid");
            }

            var userID = User.Claims.Where(a => a.Type == ClaimTypes.PrimarySid).FirstOrDefault().Value;
            var user = this._userRepo.Read(userID);

            var foundKeyword = this._keywordRepo.Get(keyword => keyword.Value == m.KeywordValue);
            if (foundKeyword.Count() > 0)
            {
                keywordId = foundKeyword.FirstOrDefault().Id;
            }

            var keyword = this._keywordRepo.Create(new Keyword()
            {
                Value = m.KeywordValue
            });
            var foundWebsite = this._websiteRepo.Get(website => website.URL == m.WebsiteURL);
            if (foundWebsite.Count() > 0)
            {
                websiteId = foundWebsite.FirstOrDefault().Id;
            }

            var website = this._websiteRepo.Create(new Website()
            {
                URL = m.WebsiteURL
            });

            var config = this._configRepo.Create(new ConfigurationJT()
            {
                WebsiteId = (websiteId == -1) ? website.Id : keywordId,
                KeywordId = keywordId == -1 ? keyword.Id : keywordId,
                UserId = userID
            });
            
            return this.Ok(new CreateConfigurationResult()
            {
                WebsiteId = (websiteId == -1) ? website.Id : keywordId,
                KeywordId = keywordId == -1 ? keyword.Id : keywordId,
                UserId = userID
            });
        }

        /*[HttpGet]
        [Route("read")]
        public async Task<IActionResult> Read()
        {
            var userID = User.Claims.Where(a => a.Type == ClaimTypes.PrimarySid).FirstOrDefault().Value;
            var userKeywords = _configRepo.Get(jt => jt.UserId == userID);
            var keywords = _keywordRepo.Get(keyword => userKeywords.Count(jt => jt.KeywordId == keyword.Id) > 0);

            var keywordList = new List<string>();
            foreach (var keyword in keywords)
            {
                keywordList.Add(keyword.Value);
            }

            var response = new ReadKeywordResult()
            {
                Keywords = keywordList
            };
            return this.Ok(response);
        }

        [HttpPatch]
        [Route("update")]
        public async Task<IActionResult> Update([FromBody] UpdateKeywordModel m)
        {
            var userID = User.Claims.Where(a => a.Type == ClaimTypes.NameIdentifier).FirstOrDefault().Value;
            
            return this.Ok();
        }

        [HttpPost]
        [Route("delete")]
        public async Task<IActionResult> Delete([FromBody] DeleteWebsiteModel model)
        {
        }*/
    }
}