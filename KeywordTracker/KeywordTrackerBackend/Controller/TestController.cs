﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;  

namespace KeywordTrackerBackend.Controller
{
    [ApiController]
    [Route("api/[controller]")]
    public class TestController : ControllerBase
    {
        [HttpPost]    
        public String GetName1() {    
            if (User.Identity.IsAuthenticated) {    
                var identity = User.Identity as ClaimsIdentity;    
                if (identity != null) {    
                    IEnumerable < Claim > claims = identity.Claims;    
                }    
                return "Valid";    
            } else {    
                return "Invalid";    
            }    
        }    
        
        [HttpPost]
        [Route("/unprotected")]
        public void Unprotected()
        {

            Console.WriteLine("Unprotected Executed");
        }

        [Authorize]
        [HttpPost]
        [Route("/protected")]
        public void Protected()
        {
            var userEmail = User.Claims.Where(a => a.Type == ClaimTypes.Email).FirstOrDefault().Value;
            Console.WriteLine($"Protected Executed {userEmail}");
        }
    }
}