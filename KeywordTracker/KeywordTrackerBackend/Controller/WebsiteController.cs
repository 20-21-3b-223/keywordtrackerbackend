using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Keywordtracker.Shared.Models;
using KeywordTrackerBackend.Domain;
using KeywordTrackerBackend.EmailService;
using KeywordTrackerBackend.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using KeywordTrackerBackend.Classes;
using Microsoft.Extensions.Configuration;

namespace KeywordTrackerBackend.Controller
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class WebsiteController : ControllerBase
    {

        private ARepository<Website> _websiteRepo;
        private ARepository<ConfigurationJT> _configRepo;
        private ARepository<User> _userRepo;
        
        public WebsiteController(ARepository<Website> websiteRepo, ARepository<ConfigurationJT> configRepo, ARepository<User> userRepo)
        {
            this._userRepo = userRepo;
            this._configRepo = configRepo;
            this._websiteRepo = websiteRepo;
        }


        [HttpPut]
        [Route("create")]
        public async Task<IActionResult> Create([FromBody] CreateWebsiteModel m)
        {
            if (!UrlHelpers.ValidHttpURL(m.WebsiteURL))
            {
                Console.WriteLine("Invalid URL");
                return this.BadRequest("URL Invalid");
            }
            
            var userID = User.Claims.Where(a => a.Type == ClaimTypes.PrimarySid).FirstOrDefault().Value;
            var user = this._userRepo.Read(userID);
            var foundWebsite = this._websiteRepo.Get(website => website.URL == m.WebsiteURL);
            if (foundWebsite.Count() > 0)
            {
                return this.Ok(new CreateWebsiteResult()
                {
                    WebsiteID = foundWebsite.FirstOrDefault().Id
                });
            }
            var website = this._websiteRepo.Create(new Website()
            {
                URL = m.WebsiteURL
            });

            return this.Ok(new CreateWebsiteResult()
            {
                WebsiteID = website.Id
            });
        }
        
        [HttpGet]
        [Route("read")]
        public async Task<IActionResult> Read()
        {
            var userID = User.Claims.Where(a => a.Type == ClaimTypes.PrimarySid).FirstOrDefault().Value;

            var userWebsites = _configRepo.Get(jt => jt.UserId == userID);
            var websites = _websiteRepo.Get(website => userWebsites.Count(jt => jt.WebsiteId == website.Id) > 0);


            var websiteList = new List<string>();
            foreach (var website in websites)
            {
                websiteList.Add(website.URL);
            }
            var response = new ReadWebsiteResult()
            {
                Websites = websiteList
            };
            
            return this.Ok(response);
        }
        
        [HttpPatch]
        [Route("update")]
        public async Task<IActionResult> Update()
        {
            var userID = User.Claims.Where(a => a.Type == ClaimTypes.NameIdentifier).FirstOrDefault().Value;
            return this.Ok();
        }
        
        [HttpPost]
        [Route("delete")]
        public async Task<IActionResult> Delete([FromBody] DeleteWebsiteModel model)
        {
            var userID = User.Claims.Where(a => a.Type == ClaimTypes.PrimarySid).FirstOrDefault().Value;
            var websiteModel = _websiteRepo.Get(w => w.URL == model.Website).FirstOrDefault();
            var userWebsites = _configRepo.Get(jt => jt.UserId == userID && websiteModel.Id == jt.WebsiteId);

            foreach (var websiteConfig in userWebsites)
            {
                _configRepo.Delete(websiteConfig);
            }

            var remaining = _configRepo.Get(jt => websiteModel.Id == jt.WebsiteId);
            if (remaining.Count() == 0)
            {
                var websites = _websiteRepo.Get(website => remaining.Count(jt => jt.WebsiteId == website.Id) > 0);
                foreach (var website in websites)
                {
                    _websiteRepo.Delete(website);
                }
            }

            return this.Ok();
        }
    }
}