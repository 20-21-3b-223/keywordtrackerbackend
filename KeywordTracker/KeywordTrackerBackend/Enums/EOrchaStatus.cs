namespace KeywordTrackerBackend.Enums
{
    public enum EOrchaStatus
    {
        Ready,
        Running,
        Stopping,
        Stopped
    }
}