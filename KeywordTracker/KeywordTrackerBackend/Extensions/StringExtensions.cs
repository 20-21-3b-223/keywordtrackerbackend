using System;

namespace KeywordTrackerBackend.Extensions
{
    public static class StringExtensions
    {
        public static string ReduceTo(this string str, int maxLength)
        {
            if (string.IsNullOrEmpty(str)) return str;

            return str.Substring(0, Math.Min(str.Length, maxLength - 3)) + "...";
        }
    }
}