using System;
using KeywordTrackerBackend.Entity;

namespace KeywordTrackerBackend.Domain
{
    public class WebsiteRepository : ARepository<Website>
    {
        public WebsiteRepository(IServiceProvider provider) : base(provider)
        {
        }
    }
}