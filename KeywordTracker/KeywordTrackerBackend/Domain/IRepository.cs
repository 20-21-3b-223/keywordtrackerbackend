using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace KeywordTrackerBackend.Domain
{
    public interface IRepository<T> where T : class
    {
        public T Read(int id);
        public T Create(T e);
        public void Update(T e);
        public void Delete(T e);
        public IEnumerable<T> Get(Expression<Func<T, bool>> expression = null);
    }
}