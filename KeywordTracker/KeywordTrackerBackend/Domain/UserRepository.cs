using System;
using KeywordTrackerBackend.Entity;

namespace KeywordTrackerBackend.Domain
{
    public class UserRepository : ARepository<User>
    {
        public UserRepository(IServiceProvider provider) : base(provider)
        {
        }
    }
}