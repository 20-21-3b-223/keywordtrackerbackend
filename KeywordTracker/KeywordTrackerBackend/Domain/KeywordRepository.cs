using System;
using KeywordTrackerBackend.Entity;

namespace KeywordTrackerBackend.Domain
{
    public class KeywordRepository : ARepository<Keyword>
    {
        public KeywordRepository(IServiceProvider provider) : base(provider)
        {
        }
    }
}