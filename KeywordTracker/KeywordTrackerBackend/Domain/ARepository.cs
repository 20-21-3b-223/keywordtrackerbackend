using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using KeywordTrackerBackend.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace KeywordTrackerBackend.Domain
{
    public abstract class ARepository<T> : IDisposable where T : class
    {
        private IServiceProvider _provider;

        protected ARepository(IServiceProvider provider)
        {
            this._provider = provider;
        }

        public void Dispose()
        {
        }

        // For testing
        protected ARepository()
        {
        }

        public virtual T Read<I>(I id)
        {
            using (var context = this._provider.GetService<KeywordTrackerDbContext>())
            {
                var _dbSet = context.Set<T>();
                return _dbSet.Find(id);
            }
        }

        public virtual T Create(T e)
        {
            using (var context = this._provider.GetService<KeywordTrackerDbContext>())
            {
                var _dbSet = context.Set<T>();
                var ent = _dbSet.Add(e).Entity;
                context.SaveChanges();
                return ent;
            }
        }

        public virtual void Update(T e)
        {
            using (var context = this._provider.GetService<KeywordTrackerDbContext>())
            {
                var _dbSet = context.Set<T>();
                _dbSet.Update(e);
                context.SaveChanges();
            }
        }

        public virtual void Delete(T e)
        {
            using (var context = this._provider.GetService<KeywordTrackerDbContext>())
            {
                var _dbSet = context.Set<T>();
                _dbSet.Remove(e);
                context.SaveChanges();
            }
        }

        public virtual IEnumerable<T> Get(Expression<Func<T, bool>> expression = null)
        {
            using (var context = this._provider.GetService<KeywordTrackerDbContext>())
            {
                var _dbSet = context.Set<T>();
                var query = _dbSet as IQueryable<T>;

                if (expression is not null) return query.Where(expression).ToList();

                return query.ToList();
            }
        }
    }
}