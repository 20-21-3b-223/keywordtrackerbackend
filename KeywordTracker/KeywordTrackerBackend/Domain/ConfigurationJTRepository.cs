using System;
using KeywordTrackerBackend.Entity;

namespace KeywordTrackerBackend.Domain
{
    public class ConfigurationJTRepository : ARepository<ConfigurationJT>
    {
        public ConfigurationJTRepository(IServiceProvider provider) : base(provider)
        {
        }
    }
}