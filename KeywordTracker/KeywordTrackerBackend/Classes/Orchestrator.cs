using System;
using KeywordTrackerBackend.Enums;
using KeywordTrackerBackend.Interfaces;
using KeywordTrackerBackend.Records;

namespace KeywordTrackerBackend.Classes
{
    public class Orchestrator : IOrchaSystem, IDisposable
    {
        public Orchestrator(INotificationSystem notificationSystem, ITrackingSystem trackingSystem)
        {
            _notificationSystem = notificationSystem;
            _trackingSystem = trackingSystem;

            _trackingSystem.TrackingResultFound += TrackingSystemOnTrackingResultFound;

            Status = EOrchaStatus.Ready;
        }

        private INotificationSystem _notificationSystem { get; }
        private ITrackingSystem _trackingSystem { get; }

        public void Dispose()
        {
            if (_trackingSystem.Status != EOrchaStatus.Stopped) _trackingSystem.Stop();
            if (_notificationSystem.Status != EOrchaStatus.Stopped) _notificationSystem.Stop();
        }

        public EOrchaStatus Status { get; set; }

        public void Start()
        {
            _notificationSystem.Start();
            _trackingSystem.Start();
            Status = EOrchaStatus.Running;
        }

        public void Stop()
        {
            Status = EOrchaStatus.Stopping;
            _notificationSystem.Stop();
            _trackingSystem.Stop();
            Status = EOrchaStatus.Stopped;
        }

        private void TrackingSystemOnTrackingResultFound(object? sender, TrackingResult e)
        {
            var request = new NotificationRequest(e.User.Email, e.User.FirstName + " " + e.User.LastName,
                new TrackingResult(e.User, e.Website, e.Keywords));
            _notificationSystem.IncomingRequests.Write(request);
        }
    }
}