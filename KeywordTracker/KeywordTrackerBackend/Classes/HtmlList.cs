﻿using System.Collections;
using System.Text;
using KeywordTrackerBackend.Records;

namespace KeywordTrackerBackend.Classes
{
    public class HtmlList
    {
        private static readonly StringBuilder _sb;

        static HtmlList()
        {
            _sb = new StringBuilder();
        }

        public HtmlList(IEnumerable content, HtmlTag innerTag, HtmlTag outerTag)
        {
            Content = content;
            InnerTag = innerTag;
            OuterTag = outerTag;
        }

        public IEnumerable Content { get; set; }
        public HtmlTag InnerTag { get; set; }
        public HtmlTag OuterTag { get; set; }

        public override string ToString()
        {
            _sb.Clear();
            _sb.Append(OuterTag.Opening);
            foreach (var value in Content)
            {
                _sb.Append(InnerTag.Opening);
                _sb.Append(value);
                _sb.Append(InnerTag.Closing);
            }

            _sb.Append(OuterTag.Closing);
            return _sb.ToString();
        }
    }
}