using System;
using System.IO;
using System.Net;
using KeywordTrackerBackend.Interfaces;

namespace KeywordTrackerBackend.Classes
{
    public class WebsiteLoader : IWebsiteLoader
    {
        public string LoadWebsite(string url)
        {
            if (!(url.StartsWith("https://") || url.StartsWith("http://"))) url = url.Insert(0, "https://");

            var request = WebRequest.Create(url);
            request.Method = "GET";

            try
            {
                var response = request.GetResponse();
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    return reader.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                return "";
            }
        }
    }
}