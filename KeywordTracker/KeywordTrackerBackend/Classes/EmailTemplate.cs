using System.Collections.Generic;
using System.IO;

namespace KeywordTrackerBackend.Classes
{
    public class EmailTemplate
    {
        private readonly Dictionary<string, string> _mappings;

        private readonly string website;

        public EmailTemplate(string path)
        {
            _mappings = new Dictionary<string, string>();
            website = new StreamReader(path).ReadToEnd();
        }

        public void AddMapping(string key, string val)
        {
            _mappings.Add(key, val);
        }

        public void RemoveMapping(string key)
        {
            _mappings.Remove(key);
        }

        public string Render()
        {
            var keys = _mappings.Keys;
            var rendered = website;
            foreach (var key in keys) rendered = rendered.Replace($"%%{key}%%", _mappings[key]);

            return rendered;
        }
    }
}