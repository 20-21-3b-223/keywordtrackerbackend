using System;
using System.Threading.Tasks;
using KeywordTrackerBackend.Interfaces;
using MailKit.Net.Smtp;
using MimeKit;

namespace KeywordTrackerBackend.Classes
{
    public class EmailClient : IEmailClient
    {
        private readonly SmtpClient _client;

        public EmailClient()
        {
            _client = new SmtpClient();
        }

        public void Authenticate(string email, string password)
        {
            _client.Authenticate(email, password);
        }

        public async Task SendEmail(MimeMessage message)
        {
            _client.Send(message);
            OnSendEmail?.Invoke(this, message);
        }

        public void Connect(string server, int port = 465)
        {
            _client.Connect(server, port);
        }

        public void Disconnect()
        {
            _client.Disconnect(true);
        }

        public EventHandler<MimeMessage> OnSendEmail { get; set; }
    }
}