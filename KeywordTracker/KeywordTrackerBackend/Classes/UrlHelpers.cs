using System;
using System.Text.RegularExpressions;

namespace KeywordTrackerBackend.Classes
{
    public static class UrlHelpers
    {
        public static bool ValidHttpURL(string s)
        {
            var valid = s.Contains("http") || s.Contains("https");

            return valid;
        }
    }
}