﻿using System;
using System.Threading;
using System.Threading.Tasks;
using KeywordTrackerBackend.Enums;
using KeywordTrackerBackend.Interfaces;
using KeywordTrackerBackend.Records;
using MimeKit;

namespace KeywordTrackerBackend.Classes
{
    public class NotificationSystem : INotificationSystem, IDisposable
    {
        private readonly IEmailClient _client;

        private readonly CancellationTokenSource _tokenSource;

        public NotificationSystem(IEmailClient client)
        {
            _tokenSource = new CancellationTokenSource();
            Status = EOrchaStatus.Ready;
            IncomingRequests = new XChannel<NotificationRequest>();
            _client = client;
        }

        public void Dispose()
        {
            _tokenSource.Cancel();
            _tokenSource.Dispose();
        }

        public XChannel<NotificationRequest> IncomingRequests { get; }
        public EOrchaStatus Status { get; private set; }

        public void Start()
        {
            _client.Connect("smtp.gmail.com");
            _client.Authenticate("keywordtrackersite@gmail.com", "F7fknbeZNEkNcW6");

            ConsumeIncomingRequests();
            Status = EOrchaStatus.Running;
        }

        public void Stop()
        {
            _tokenSource.Cancel();
            _client.Disconnect();

            Status = EOrchaStatus.Stopped;
        }

        private void ConsumeIncomingRequests()
        {
            var token = _tokenSource.Token;

            Action consumeQueue = async () =>
            {
                while (!token.IsCancellationRequested)
                {
                    var request = await IncomingRequests.Read();

                    SendEmail(request);
                }
            };

            Task.Factory.StartNew(consumeQueue);
        }

        public void AddNotification(NotificationRequest request)
        {
            IncomingRequests.Write(request);
        }

        private void SendEmail(NotificationRequest request)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("Team Keyword-Tracker", "keywordtrackersite@gmail.com"));
            message.To.Add(new MailboxAddress(request.Name, request.Email));
            message.Subject = "KeywordTracker.at - Result";

            var bodyBuilder = new BodyBuilder();

            var template = new EmailTemplate("Templates/default-email.html");

            template.AddMapping("USER", request.Name);
            template.AddMapping("WEBSITE", request.Result.Website);
            template.AddMapping("CONTEXT",
                new HtmlList(request.Result.Keywords, new HtmlTag("<p>", "</p>"), new HtmlTag("", ""))
                    .ToString());

            bodyBuilder.HtmlBody = template.Render();

            message.Body = bodyBuilder.ToMessageBody();

            _client.SendEmail(message);
        }
    }
}