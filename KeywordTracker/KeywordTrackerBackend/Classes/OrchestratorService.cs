﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace KeywordTrackerBackend.Classes
{
    public class OrchestratorService : IHostedService, IDisposable
    {

        private Orchestrator _orchestrator;
        private IServiceScope scope;
        
        public OrchestratorService(IServiceProvider provider)
        {
            scope = provider.CreateScope();
            _orchestrator = scope.ServiceProvider.GetService<Orchestrator>();
        }
        
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            _orchestrator.Start();
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _orchestrator.Stop();
        }

        public void Dispose()
        {
            _orchestrator?.Dispose();
            this.scope?.Dispose();
        }
    }
}