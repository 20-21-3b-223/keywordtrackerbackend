using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using HtmlAgilityPack;
using KeywordTrackerBackend.Domain;
using KeywordTrackerBackend.Entity;
using KeywordTrackerBackend.Enums;
using KeywordTrackerBackend.Exceptions;
using KeywordTrackerBackend.Interfaces;
using KeywordTrackerBackend.Records;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace KeywordTrackerBackend.Classes
{
    public class TrackingSystem : ITrackingSystem, IDisposable
    {
        private readonly IWebsiteLoader _loader;
        private readonly ILogger<TrackingSystem> _logger;
        private CancellationTokenSource _source;
        private readonly IServiceScopeFactory scopeFactory;


        public TrackingSystem(ILogger<TrackingSystem> logger, IWebsiteLoader loader, IServiceScopeFactory scopeFactory)
        {
            this.scopeFactory = scopeFactory;
            Interval = 30000;
            _logger = logger;
            _source = new CancellationTokenSource();
            Status = EOrchaStatus.Ready;
            TrackingResultOutChannel = new XChannel<TrackingResult>();
            _loader = loader;
        }

        public void Dispose()
        {
            _source.Cancel();
            _source.Dispose();
        }

        public EOrchaStatus Status { get; private set; }
        public int Interval { get; set; }
        public XChannel<TrackingResult> TrackingResultOutChannel { get; }
        public event EventHandler<TrackingResult> TrackingResultFound;

        public void Start()
        {
            _logger.Log(LogLevel.Information, "Starting Tracking Process");
            if (!(Status == EOrchaStatus.Ready || Status == EOrchaStatus.Stopped))
            {
                _logger.Log(LogLevel.Error, $"Tracking System is not ready - Status : {Status}");
                throw new TrackingException($"Tracking System is not ready - Status : {Status}");
            }

            Status = EOrchaStatus.Running;
            Task.Factory.StartNew(this.StartProcess);
        }

        public void Stop()
        {
            _logger.Log(LogLevel.Information, "Stopping Tracking Process");
            if (Status != EOrchaStatus.Running)
            {
                _logger.Log(LogLevel.Error, $"Tracking System is not running - Status : {Status}");
                throw new TrackingException($"Tracking System is not running - Status : {Status}");
            }

            Status = EOrchaStatus.Stopping;
            _source.Cancel();
            _source = new CancellationTokenSource();
        }

        private async void StartProcess()
        {
            using (var scope = scopeFactory.CreateScope())
            {
                // var _userRepo = scope.ServiceProvider.GetRequiredService<ARepository<User>>();
                // var _configJTRepo = scope.ServiceProvider.GetRequiredService<ARepository<ConfigurationJT>>();
                // var _websiteRepo = scope.ServiceProvider.GetRequiredService<ARepository<Website>>();
                // var _keywordRepo = scope.ServiceProvider.GetRequiredService<ARepository<Keyword>>();
                
                var token = _source.Token;
                while (!token.IsCancellationRequested)
                {
                    var intervalDelay = Task.Delay(Interval);
                    _logger.Log(LogLevel.Information, "Starting Processing");
                    var perfCounter = new Stopwatch();
                    perfCounter.Restart();
                    var totalRequestCount = 0;
                    var websiteCache = new ConcurrentDictionary<string, string>();
                    var keywordsFoundPerUser =
                        new ConcurrentDictionary<User, Dictionary<string, IEnumerable<HtmlNode>>>();
                    IEnumerable<User> users;
                    using var _userRepo = scope.ServiceProvider.GetService<ARepository<User>>();
                    {
                        users = _userRepo.Get();
                    }
                    foreach(var user in users)
                    {
                        IEnumerable<ConfigurationJT> configs;
                        using var _configJTRepo = scope.ServiceProvider.GetService<ARepository<ConfigurationJT>>();
                        {
                            configs = _configJTRepo.Get();
                        }
                        var websiteIds = from cr in configs
                            where cr.UserId == user.Id
                            select cr.WebsiteId;


                        foreach (var websiteId in websiteIds)
                        {
                            Website website;
                            using var _websiteRepo = scope.ServiceProvider.GetService<ARepository<Website>>();
                            {
                                website = _websiteRepo.Read(websiteId);
                            }

                            string htmlWebsite;

                            if (websiteCache.ContainsKey(website.URL))
                            {
                                websiteCache.TryGetValue(website.URL, out htmlWebsite);
                            }
                            else
                            {
                                totalRequestCount += 1;
                                htmlWebsite = _loader.LoadWebsite(website.URL);
                                if (htmlWebsite == "")
                                {
                                    continue;
                                }
                                websiteCache.TryAdd(website.URL, htmlWebsite);
                            }

                            var element = new HtmlDocument();
                            element.LoadHtml(htmlWebsite);

                            var keywordIds = from cr in _configJTRepo.Get()
                                where cr.UserId == user.Id && cr.WebsiteId == websiteId
                                select cr.KeywordId;

                            foreach (var keywordId in keywordIds)
                            {
                                Keyword keyword;
                                using var _keywordRepo = scope.ServiceProvider.GetService<ARepository<Keyword>>();
                                {
                                    keyword = _keywordRepo.Read(keywordId);
                                }

                                var nodesWithText = from node in element.DocumentNode.SelectNodes("//text()")
                                    where node.InnerText.Contains(keyword.Value,
                                        StringComparison.InvariantCultureIgnoreCase)
                                    select node;

                                if (nodesWithText.Count() > 0)
                                {
                                    if (!keywordsFoundPerUser.ContainsKey(user))
                                        keywordsFoundPerUser.TryAdd(user,
                                            new Dictionary<string, IEnumerable<HtmlNode>>());

                                    keywordsFoundPerUser[user].TryAdd(keyword.Value, nodesWithText);

                                    var keywords = new Keyword[nodesWithText.Count()];

                                    var temp = nodesWithText.ToArray();
                                    for (var i = 0; i < nodesWithText.Count(); i++)
                                        keywords[i] = new Keyword
                                        {
                                            Value = keyword.Value,
                                            ContextValue = temp[i].InnerText,
                                            XPath = temp[i].XPath
                                        };

                                    var trackingResult = new TrackingResult(user, website.URL, keywords);
                                    TrackingResultFound?.Invoke(this, trackingResult);
                                    TrackingResultOutChannel.Write(trackingResult);
                                }
                            }
                        }
                    };

                    perfCounter.Stop();
                    _logger.Log(LogLevel.Information,
                        $"Finished Processing Time Spent: {perfCounter.ElapsedMilliseconds}ms Total Webcalls: {totalRequestCount}");
                    await intervalDelay;
                }

                Status = EOrchaStatus.Stopped;
            }
        }
    }
}