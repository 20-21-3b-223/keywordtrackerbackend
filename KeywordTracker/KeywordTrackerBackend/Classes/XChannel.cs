using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace KeywordTrackerBackend.Classes
{
    public class XChannel<T>
    {
        private readonly Channel<T> _channel;
        private readonly ChannelReader<T> _reader;
        private readonly CancellationTokenSource _tokenSource;

        public XChannel()
        {
            _channel = Channel.CreateUnbounded<T>();
            _tokenSource = new CancellationTokenSource();
            _reader = _channel.Reader;
        }

        public async void Write(T obj)
        {
            await _channel.Writer.WriteAsync(obj, _tokenSource.Token);
        }

        public async ValueTask<T> Read()
        {
            return await _reader.ReadAsync(_tokenSource.Token);
        }

        public bool TryRead(out T item)
        {
            if (_reader.TryRead(out var i))
            {
                item = i;
                return true;
            }

            item = default;
            return false;
        }

        public void CancelChannel()
        {
            _tokenSource.Cancel();
        }

        public int Count()
        {
            return _channel.Reader.Count;
        }
    }
}