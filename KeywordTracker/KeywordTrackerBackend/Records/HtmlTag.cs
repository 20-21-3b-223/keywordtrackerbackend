﻿namespace KeywordTrackerBackend.Records
{
    public record HtmlTag(string Opening, string Closing);
}