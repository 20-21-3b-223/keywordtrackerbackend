﻿using KeywordTrackerBackend.Records;

namespace KeywordTrackerBackend.Classes
{
    public record NotificationRequest(string? Email, string? Name, TrackingResult? Result);
}