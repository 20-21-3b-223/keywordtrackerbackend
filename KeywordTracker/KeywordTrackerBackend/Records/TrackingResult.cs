﻿using KeywordTrackerBackend.Entity;

namespace KeywordTrackerBackend.Records
{
    public record TrackingResult(User? User, string? Website, Keyword[]? Keywords);
}