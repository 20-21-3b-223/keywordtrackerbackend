﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KeywordTrackerBackend.Entity
{
    [Table("KEYWORDS")]
    public class Keyword
    {
        [Key]
        [Column("ID", TypeName = "INT")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("VALUE", TypeName = "VARCHAR(255)")]
        public string Value { get; set; }

        [NotMapped] public string ContextValue { get; set; } = "";

        [NotMapped] public string XPath { get; set; } = "";

        public override string ToString()
        {
            return
                $"<u>{nameof(Value)}</u>: <span style='color:black'>{Value}</span><br> <u>{nameof(ContextValue)}</u>:<br><span style='color:black'> - {ContextValue}</span>";
        }
    }
}