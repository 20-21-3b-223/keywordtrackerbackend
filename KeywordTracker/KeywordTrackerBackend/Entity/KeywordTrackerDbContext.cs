﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace KeywordTrackerBackend.Entity
{
    public class KeywordTrackerDbContext : IdentityDbContext<User>
    {
        public KeywordTrackerDbContext(DbContextOptions<KeywordTrackerDbContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Website> Websites { get; set; }
        public DbSet<Keyword> Keywords { get; set; }
        public DbSet<ConfigurationJT> ConfigurationJTs { get; set; }

        protected override void OnModelCreating(ModelBuilder mb)
        {
            base.OnModelCreating(mb);

            mb.Entity<ConfigurationJT>()
                .HasKey(c => new {c.KeywordId, c.WebsiteId, c.UserId});

            mb.Entity<ConfigurationJT>()
                .HasOne(c => c.User)
                .WithMany()
                .HasForeignKey(c => c.UserId);
            mb.Entity<ConfigurationJT>()
                .HasOne(c => c.Website)
                .WithMany()
                .HasForeignKey(c => c.WebsiteId);
            mb.Entity<ConfigurationJT>()
                .HasOne(c => c.Keyword)
                .WithMany()
                .HasForeignKey(c => c.KeywordId);
        }
    }
}