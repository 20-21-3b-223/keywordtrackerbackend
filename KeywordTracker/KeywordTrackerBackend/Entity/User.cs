﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace KeywordTrackerBackend.Entity
{
    [Table("USERS")]
    public class User : IdentityUser
    {
        [Required]
        [Column("FIRST_NAME", TypeName = "VARCHAR(100)")]
        public string FirstName { get; set; }

        [Required]
        [Column("LAST_NAME", TypeName = "VARCHAR(100)")]
        public string LastName { get; set; }

        [Column("JOIN_DATE")] public DateTime JoinDate { get; set; }
    }
}