﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KeywordTrackerBackend.Entity
{
    [Table("WEBSITES")]
    public class Website
    {
        [Key]
        [Column("ID", TypeName = "INT")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Column("URL", TypeName = "LONGTEXT")]
        public string URL { get; set; }
    }
}