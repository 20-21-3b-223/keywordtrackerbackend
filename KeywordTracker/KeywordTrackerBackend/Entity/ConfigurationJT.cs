using System.ComponentModel.DataAnnotations.Schema;

namespace KeywordTrackerBackend.Entity
{
    [Table("CONFIGURATION_JTS")]
    public class ConfigurationJT
    {
        [Column("USER_ID")] public string UserId { get; set; }

        [Column("WEBSITE_ID")] public int WebsiteId { get; set; }

        [Column("KEYWORD_ID")] public int KeywordId { get; set; }

        public User User { get; set; }
        public Website Website { get; set; }
        public Keyword Keyword { get; set; }
    }
}