using System;

namespace KeywordTrackerBackend.Exceptions
{
    public class TrackingException : Exception
    {
        public TrackingException(string message) : base(message)
        {
        }
    }
}