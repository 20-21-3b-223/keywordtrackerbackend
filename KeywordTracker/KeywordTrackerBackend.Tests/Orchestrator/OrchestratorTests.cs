using System.Threading.Tasks;
using KeywordTrackerBackend.Domain;
using KeywordTrackerBackend.Entity;
using KeywordTrackerBackend.Enums;
using KeywordTrackerBackend.Interfaces;
using KeywordTrackerBackend.Tests.FakeDatabase;
using KeywordTrackerBackend.Tests.NotificationSystem;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Xunit;
using Xunit.Abstractions;

namespace KeywordTrackerBackend.Tests.Orchestrator
{
    public class OrchestratorTests
    {
        private readonly ITestOutputHelper _output;

        public OrchestratorTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public void IntegrationTest()
        {
            var provider = new ServiceCollection()
                .AddLogging(builder => builder.AddXunit(_output))
                .AddSingleton<INotificationSystem, Classes.NotificationSystem>()
                .AddSingleton<ITrackingSystem, Classes.TrackingSystem>()
                .AddSingleton<Classes.Orchestrator>()
                .AddSingleton<IWebsiteLoader, TestWebsiteLoader>()
                .AddSingleton<IEmailClient, FakeEmailClient>()
                .AddSingleton<ARepository<ConfigurationJT>, FakeConfigJT>()
                .AddSingleton<ARepository<Keyword>, FakeKeywordRepo>()
                .AddSingleton<ARepository<Website>, FakeWebsiteRepo>()
                .AddSingleton<ARepository<User>, FakeUserRepo>()
                .BuildServiceProvider();

            using var orchestrator = provider.GetService<Classes.Orchestrator>();
            {
                var receiveCount = 0;

                var emailclient = provider.GetService<IEmailClient>();
                var trackingsystem = provider.GetService<ITrackingSystem>();
                var notificationsystem = provider.GetService<INotificationSystem>();

                emailclient.OnSendEmail += (sender, message) =>
                {
                    _output.WriteLine($"Received Email to: {message.To}");
                    receiveCount += 1;
                };

                Assert.True(trackingsystem.Status == EOrchaStatus.Ready);
                Assert.True(notificationsystem.Status == EOrchaStatus.Ready);

                orchestrator.Start();
                var taskDelay = Task.Delay(60000);

                Assert.True(trackingsystem.Status == EOrchaStatus.Running);
                Assert.True(notificationsystem.Status == EOrchaStatus.Running);


                taskDelay.GetAwaiter().GetResult();

                Assert.True(receiveCount > 0);
            }
        }
    }
}