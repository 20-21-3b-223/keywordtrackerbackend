﻿using System.Threading.Tasks;
using KeywordTrackerBackend.Classes;
using KeywordTrackerBackend.Entity;
using KeywordTrackerBackend.Interfaces;
using KeywordTrackerBackend.Records;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Xunit;
using Xunit.Abstractions;

namespace KeywordTrackerBackend.Tests.NotificationSystem
{
    public class NotificationSystemTest
    {
        private readonly ITestOutputHelper _output;

        public NotificationSystemTest(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public async void SendEmailTest()
        {
            var provider = new ServiceCollection()
                .AddLogging(builder => builder.AddXunit(_output))
                .AddSingleton<Classes.NotificationSystem>()
                .AddSingleton<IEmailClient, EmailClient>()
                .BuildServiceProvider();
            
            using var notificationSystem = provider.GetService<Classes.NotificationSystem>();
            {
                var received = false;

                var emailClient = provider.GetService<IEmailClient>();
                emailClient.OnSendEmail += (sender, message) => received = true;

                var taskDelay = Task.Delay(5000);

                notificationSystem.AddNotification(new NotificationRequest
                (
                    "k.kanzler@htlkrems.at",
                    "Konstantin Kanzler",
                    new TrackingResult
                    (
                        null,
                        "https://orf.at/",
                        new[]
                        {
                            new Keyword
                            {
                                Value = "Tor",
                                ContextValue = "Goldenes Baumgartner-Tor: Österreich folgt Oranje ins Achtelfinale",
                                XPath = "/xpaaath/"
                            }
                        }
                    )
                ));

                notificationSystem?.Start();

                await taskDelay;

                notificationSystem?.Stop();

                Assert.True(received);
            }
        }
    }
}