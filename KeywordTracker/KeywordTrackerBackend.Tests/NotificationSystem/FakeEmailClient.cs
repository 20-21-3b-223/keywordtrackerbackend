using System;
using System.Threading.Tasks;
using KeywordTrackerBackend.Interfaces;
using Microsoft.Extensions.Logging;
using MimeKit;

namespace KeywordTrackerBackend.Tests.NotificationSystem
{
    public class FakeEmailClient : IEmailClient
    {
        private readonly ILogger<FakeEmailClient>? _logger;

        public FakeEmailClient(ILogger<FakeEmailClient> logger = null)
        {
            _logger = logger;
        }

        public async Task SendEmail(MimeMessage message)
        {
            OnSendEmail.Invoke(this, message);
            _logger?.LogInformation("Sending Message");
        }

        public void Connect(string server, int port = 465)
        {
            _logger?.LogInformation("Connecting");
        }

        public void Disconnect()
        {
            _logger?.LogInformation("Disconnecting");
        }

        public void Authenticate(string email, string password)
        {
            _logger?.LogInformation("Authenticating");
        }

        public EventHandler<MimeMessage> OnSendEmail { get; set; }
    }
}