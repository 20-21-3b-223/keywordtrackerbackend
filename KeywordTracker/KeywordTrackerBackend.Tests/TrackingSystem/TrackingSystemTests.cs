using System.Threading.Tasks;
using KeywordTrackerBackend.Classes;
using KeywordTrackerBackend.Domain;
using KeywordTrackerBackend.Entity;
using KeywordTrackerBackend.Extensions;
using KeywordTrackerBackend.Interfaces;
using KeywordTrackerBackend.Tests.FakeDatabase;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Xunit;
using Xunit.Abstractions;

namespace KeywordTrackerBackend.Tests.TrackingSystem
{
    public class TrackingSystemTests
    {
        private readonly ITestOutputHelper _output;

        public TrackingSystemTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public async void DownloadWebsiteTest()
        {
            var provider = new ServiceCollection()
                .AddLogging(builder => builder.AddXunit(_output))
                .AddSingleton<IWebsiteLoader, WebsiteLoader>()
                .AddSingleton<Classes.TrackingSystem>()
                .AddSingleton<IWebsiteLoader, TestWebsiteLoader>()
                .AddSingleton<ARepository<ConfigurationJT>, FakeConfigJT>()
                .AddSingleton<ARepository<Keyword>, FakeKeywordRepo>()
                .AddSingleton<ARepository<Website>, FakeWebsiteRepo>()
                .AddSingleton<ARepository<User>, FakeUserRepo>()
                .BuildServiceProvider();

            using var trackingSystem = provider.GetService<Classes.TrackingSystem>();
            {
                var taskDelay = Task.Delay(30000);

                trackingSystem.Start();

                await taskDelay;

                trackingSystem.Stop();
            }
        }

        [Fact]
        public void EventHandlerTests()
        {
            var provider = new ServiceCollection()
                .AddLogging(builder => builder.AddXunit(_output))
                .AddSingleton<Classes.TrackingSystem>()
                .AddSingleton<IWebsiteLoader, TestWebsiteLoader>()
                .AddSingleton<ARepository<ConfigurationJT>, FakeConfigJT>()
                .AddSingleton<ARepository<Keyword>, FakeKeywordRepo>()
                .AddSingleton<ARepository<Website>, FakeWebsiteRepo>()
                .AddSingleton<ARepository<User>, FakeUserRepo>()
                .BuildServiceProvider();


            using var trackingSystem = provider.GetService<Classes.TrackingSystem>();
            {
                trackingSystem.Interval = 10000;
                var taskDelay = Task.Delay(30000);

                trackingSystem.TrackingResultFound += (sender, result) =>
                {
                    var output = $"User: {result.User.Email} Website: {result.Website} ";
                    foreach (var keyword in result.Keywords)
                        output +=
                            $"Keyword: {keyword.Value} Context: {keyword.ContextValue.ReduceTo(50)} XPath: {keyword.XPath.ReduceTo(50)} \n";

                    _output.WriteLine(output);
                };

                trackingSystem.Start();

                taskDelay.GetAwaiter().GetResult();

                trackingSystem.Stop();
            }
        }
    }
}