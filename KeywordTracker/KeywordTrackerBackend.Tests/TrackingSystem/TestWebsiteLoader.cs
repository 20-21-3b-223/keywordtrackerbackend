using System.Collections.Generic;
using System.IO;
using KeywordTrackerBackend.Interfaces;

namespace KeywordTrackerBackend.Tests
{
    public class TestWebsiteLoader : IWebsiteLoader
    {
        private readonly Dictionary<string, string> _websites;

        public TestWebsiteLoader()
        {
            _websites = new Dictionary<string, string>();

            var files = Directory.GetFiles("TrackingSystem\\TestData");

            foreach (var file in files)
                if (file.EndsWith(".html"))
                {
                    var fileContent = File.ReadAllText(file);
                    _websites.Add(file.Replace(".html", "").Replace("TrackingSystem\\TestData\\", ""),
                        fileContent);
                }
        }

        public string LoadWebsite(string url)
        {
            if (url.Contains("https://") || url.Contains("http://"))
                url = url.Replace("https://", "").Replace("http://", "");

            return _websites[url];
        }
    }
}