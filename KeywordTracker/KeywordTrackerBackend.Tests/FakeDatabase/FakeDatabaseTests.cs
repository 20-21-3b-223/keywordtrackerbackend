using KeywordTrackerBackend.Domain;
using KeywordTrackerBackend.Entity;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using Xunit.Abstractions;

namespace KeywordTrackerBackend.Tests.FakeDatabase
{
    public class FakeDatabaseTests
    {
        private readonly ITestOutputHelper _output;

        public FakeDatabaseTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public void FakeUserRepoTest()
        {
            var provider = new ServiceCollection()
                .AddSingleton<ARepository<User>, FakeUserRepo>()
                .BuildServiceProvider();

            var userRepo = provider.GetService<ARepository<User>>();
            _output.WriteLine(userRepo.Read("a").FirstName);
        }

        [Fact]
        public void FakeWebsiteRepoTest()
        {
            var provider = new ServiceCollection()
                .AddSingleton<ARepository<Website>, FakeWebsiteRepo>()
                .BuildServiceProvider();

            var websiteRepo = provider.GetService<ARepository<Website>>();
            _output.WriteLine(websiteRepo.Read(1).URL);
        }

        [Fact]
        public void FakeKeywordRepoTest()
        {
            var provider = new ServiceCollection()
                .AddSingleton<ARepository<Keyword>, FakeKeywordRepo>()
                .BuildServiceProvider();

            var keywordRepo = provider.GetService<ARepository<Keyword>>();
            _output.WriteLine(keywordRepo.Read(1).Value);
        }

        [Fact]
        public void FakeConfigRepoTest()
        {
            var provider = new ServiceCollection()
                .AddSingleton<ARepository<ConfigurationJT>, FakeConfigJT>()
                .BuildServiceProvider();

            var configRepo = provider.GetService<ARepository<ConfigurationJT>>();
            _output.WriteLine(configRepo.Read(1).UserId);
        }
    }
}