using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using KeywordTrackerBackend.Domain;
using KeywordTrackerBackend.Entity;

namespace KeywordTrackerBackend.Tests.FakeDatabase
{
    public class FakeKeywordRepo : ARepository<Keyword>
    {
        private readonly List<Keyword> _keywords;

        public FakeKeywordRepo()
        {
            _keywords = new List<Keyword>
            {
                new()
                {
                    Id = 1,
                    Value = "corona"
                },
                new()
                {
                    Id = 2,
                    Value = "kurz"
                },
                new()
                {
                    Id = 3,
                    Value = "minecraft"
                },
                new()
                {
                    Id = 4,
                    Value = "csgo"
                }
            };
        }

        public override Keyword Read<I>(I id)
        {
            return _keywords.Find(keyword => keyword.Id == Convert.ToInt32(id));
        }

        public override Keyword Create(Keyword e)
        {
            _keywords.Add(e);
            return e;
        }

        public override void Update(Keyword e)
        {
            var id = _keywords.FindIndex(keyword => keyword.Id == e.Id);
            _keywords[id] = e;
        }

        public override void Delete(Keyword e)
        {
            _keywords.Remove(e);
        }

        public override IEnumerable<Keyword> Get(Expression<Func<Keyword, bool>> expression = null)
        {
            var query = _keywords as IQueryable<Keyword>;

            if (expression is not null) return query.Where(expression);

            return query.ToList();
        }
    }
}