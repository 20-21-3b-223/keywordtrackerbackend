using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using KeywordTrackerBackend.Domain;
using KeywordTrackerBackend.Entity;

namespace KeywordTrackerBackend.Tests.FakeDatabase
{
    public class FakeConfigJT : ARepository<ConfigurationJT>
    {
        private readonly List<ConfigurationJT> _configurations;

        public FakeConfigJT()
        {
            _configurations = new List<ConfigurationJT>
            {
                new()
                {
                    UserId = "a",
                    WebsiteId = 3,
                    KeywordId = 1,
                    User = new User
                    {
                        Id = "a",
                        Email = "jay@jinxx.eu",
                        FirstName = "jay",
                        LastName = "leimer"
                    },
                    Website = new Website
                    {
                        Id = 3,
                        URL = "http://orf.at"
                    },
                    Keyword = new Keyword
                    {
                        Id = 1,
                        Value = "corona"
                    }
                }
            };
        }

        public override ConfigurationJT Read<I>(I id)
        {
            return _configurations.First();
        }

        public override ConfigurationJT Create(ConfigurationJT e)
        {
            _configurations.Add(e);
            return e;
        }

        public override void Update(ConfigurationJT e)
        {
            var id = _configurations.FindIndex(conf =>
                conf.KeywordId == e.KeywordId && conf.UserId == e.UserId && conf.WebsiteId == e.WebsiteId);
            _configurations[id] = e;
        }

        public override void Delete(ConfigurationJT e)
        {
            _configurations.Remove(e);
        }

        public override IEnumerable<ConfigurationJT> Get(Expression<Func<ConfigurationJT, bool>> expression = null)
        {
            var query = _configurations.AsQueryable();

            if (expression is not null) return query.Where(expression);

            return query.ToList();
        }
    }
}