using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using KeywordTrackerBackend.Domain;
using KeywordTrackerBackend.Entity;

namespace KeywordTrackerBackend.Tests.FakeDatabase
{
    public class FakeUserRepo : ARepository<User>
    {
        private readonly List<User> _users;

        public FakeUserRepo()
        {
            _users = new List<User>
            {
                new()
                {
                    Id = "a",
                    Email = "jay@jinxx.eu",
                    FirstName = "jay",
                    LastName = "leimer"
                },
                new()
                {
                    Id = "b",
                    Email = "alex@jinxx.eu",
                    FirstName = "alex",
                    LastName = "dick"
                },
                new()
                {
                    Id = "c",
                    Email = "emi@jinxx.eu",
                    FirstName = "emi",
                    LastName = "ardelean"
                },
                new()
                {
                    Id = "d",
                    Email = "konsti@jinxx.eu",
                    FirstName = "konstantin",
                    LastName = "kanzler"
                }
            };
        }

        public override User Read<I>(I id)
        {
            return _users.Find(user => user.Id == id as string);
        }

        public override User Create(User e)
        {
            _users.Add(e);
            return e;
        }

        public override void Update(User e)
        {
            var id = _users.FindIndex(user => user.Id == e.Id);
            _users[id] = e;
        }

        public override void Delete(User e)
        {
            _users.Remove(e);
        }

        public override IEnumerable<User> Get(Expression<Func<User, bool>> expression = null)
        {
            var query = _users.AsQueryable();

            if (expression is not null) return query.Where(expression);

            return query.ToList();
        }
    }
}