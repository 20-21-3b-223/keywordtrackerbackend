using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using KeywordTrackerBackend.Domain;
using KeywordTrackerBackend.Entity;

namespace KeywordTrackerBackend.Tests.FakeDatabase
{
    public class FakeWebsiteRepo : ARepository<Website>
    {
        private readonly List<Website> _websites;

        public FakeWebsiteRepo()
        {
            _websites = new List<Website>
            {
                new()
                {
                    Id = 1,
                    URL = "http://gitlab.com"
                },
                new()
                {
                    Id = 2,
                    URL = "http://mudblazor.com"
                },
                new()
                {
                    Id = 3,
                    URL = "http://orf.at"
                },
                new()
                {
                    Id = 4,
                    URL = "http://youtube.com"
                }
            };
        }

        public override Website Read<I>(I id)
        {
            return _websites.Find(website => website.Id == Convert.ToInt32(id));
        }

        public override Website Create(Website e)
        {
            _websites.Add(e);
            return e;
        }

        public override void Update(Website e)
        {
            var id = _websites.FindIndex(website => website.Id == e.Id);
            _websites[id] = e;
        }

        public override void Delete(Website e)
        {
            _websites.Remove(e);
        }

        public override IEnumerable<Website> Get(Expression<Func<Website, bool>> expression = null)
        {
            var query = _websites as IQueryable<Website>;

            if (expression is not null) return query.Where(expression);

            return query.ToList();
        }
    }
}