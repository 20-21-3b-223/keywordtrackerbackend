﻿using System.Collections;
using System.Collections.Generic;

namespace Keywordtracker.Shared.Models
{
    public class GetKeywordResult
    {
        public IEnumerable<KeywordResult> Results { get; set; }
    }

    public record KeywordResult(string Website, string Keyword);
}