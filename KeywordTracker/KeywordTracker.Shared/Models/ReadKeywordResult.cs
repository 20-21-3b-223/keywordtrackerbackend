﻿using System.Collections.Generic;

namespace Keywordtracker.Shared.Models
{
    public class ReadKeywordResult
    {
        public IEnumerable<string> Keywords { get; set; }
    }
}