﻿namespace Keywordtracker.Shared.Models
{
    public class DeleteKeywordModel
    {
        public string Keyword { get; set; }
        public string Website { get; set; }
    }
}