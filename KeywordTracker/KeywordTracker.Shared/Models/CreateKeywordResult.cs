﻿namespace Keywordtracker.Shared.Models
{
    public class CreateKeywordResult
    {
        public int KeywordId { get; set; }
    }
}