namespace Keywordtracker.Shared.Models
{
    public class CreateWebsiteResult
    {
        public int WebsiteID { get; set; }
    }
}