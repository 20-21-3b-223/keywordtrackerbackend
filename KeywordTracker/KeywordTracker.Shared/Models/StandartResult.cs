﻿using System.Collections.Generic;

namespace Keywordtracker.Shared.Models
{
    public class StandartResult
    {
        public bool Successful { get; set; }
        public IEnumerable<string> Errors { get; set; }
    }
}
