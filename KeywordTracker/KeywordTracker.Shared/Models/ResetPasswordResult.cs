﻿using System.Collections.Generic;

namespace Keywordtracker.Shared.Models{
    public class ResetPasswordResult{
        public bool Successful { get; set; }
        public IEnumerable<string> Error { get; set; }
    }
}