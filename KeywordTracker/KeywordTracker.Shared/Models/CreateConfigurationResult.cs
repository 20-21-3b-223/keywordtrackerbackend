﻿namespace Keywordtracker.Shared.Models
{
    public class CreateConfigurationResult
    {
        public int KeywordId { get; set; }
        public int WebsiteId { get; set; }
        public string UserId { get; set; }
    }
}