﻿using System.ComponentModel.DataAnnotations;

namespace Keywordtracker.Shared.Models
{
    public class UpdateKeywordModel
    {
        [Required] [DataType(DataType.Text)] public string KeywordValue { get; set; }

        [Required] [DataType(DataType.Text)] public string KeywordValueOld { get; set; }
    }
}