using System.ComponentModel.DataAnnotations;

namespace Keywordtracker.Shared.Models
{
    public class CreateWebsiteModel
    {
        [Required]
        [DataType(DataType.Url)]
        public string WebsiteURL { get; set; }
    }
}