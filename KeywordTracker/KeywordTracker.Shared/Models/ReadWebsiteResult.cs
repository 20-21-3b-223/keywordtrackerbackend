﻿using System.Collections;
using System.Collections.Generic;

namespace Keywordtracker.Shared.Models
{
    public class ReadWebsiteResult
    {
        public IEnumerable<string> Websites { get; set; }
    }
}