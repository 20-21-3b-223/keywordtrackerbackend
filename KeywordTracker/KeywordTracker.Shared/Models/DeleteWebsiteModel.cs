﻿using System.ComponentModel.DataAnnotations;

namespace Keywordtracker.Shared.Models
{
    public class DeleteWebsiteModel
    {
        [Required]
        public string Website { get; set; }
    }
}