using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Keywordtracker.Shared.Models{
    public class RegisterModel{
        [Required(ErrorMessage = "First Name is required")]  
        [Display(Name = "FirstName")]
        public string FirstName { get; set; }  
        
        [Required(ErrorMessage = "Last Name is required")]  
        [Display(Name = "LastName")]
        public string LastName { get; set; }  
  
        [EmailAddress]  
        [Display(Name = "Email")]
        [Required(ErrorMessage = "Email is required")]  
        public string Email { get; set; }  
  
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}
