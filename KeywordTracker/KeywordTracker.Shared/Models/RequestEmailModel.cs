﻿using System.ComponentModel.DataAnnotations;

namespace Keywordtracker.Shared.Models {
    public class RequestEmailModel {
        
        [Required]
        [DataType(DataType.EmailAddress)]
        public string NewEmail { get; set; }
        
    }
}