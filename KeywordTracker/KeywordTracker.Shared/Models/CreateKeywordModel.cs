﻿using System.ComponentModel.DataAnnotations;

namespace Keywordtracker.Shared.Models
{
    public class CreateKeywordModel
    {
        [Required] 
        [DataType(DataType.Text)] 
        public string KeywordValue { get; set; }
        
        [Required]
        [DataType(DataType.Url)]
        public string WebsiteURL { get; set; }
    }
}