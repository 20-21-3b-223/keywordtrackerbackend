﻿using System;
using Microsoft.VisualBasic;

namespace Keywordtracker.Shared.Models {
    public class EmailNameModel {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime JoinedAt { get; set; }
        public string Email { get; set; }
    }
}