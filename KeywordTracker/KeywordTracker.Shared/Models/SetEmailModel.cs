﻿using System.ComponentModel.DataAnnotations;

namespace Keywordtracker.Shared.Models {
    public class SetEmailModel {
        [Required]public string Email { get; set; }
        [Required]public string Token { get; set; }
        [Required]public string NewEmail { get; set; }
    }
}