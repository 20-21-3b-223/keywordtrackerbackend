﻿namespace Keywordtracker.Shared.Models{
    public class ConfirmPasswordModel{
        public string Email { get; set; }
        public string Token { get; set; }
    }
}